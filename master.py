import canopen
import time
import logging
import threading

#logging.basicConfig(level=logging.DEBUG)


network = canopen.Network()
network.connect(bustype='socketcan', channel='can0')
network.check()

node = canopen.RemoteNode(35, 'eds/e35.eds')
network.add_node(node)
		
node.nmt.state = 'PRE-OPERATIONAL'
node.rpdo.read()
node.tpdo.read()
node.nmt.state = 'OPERATIONAL'

limit = 1000
milestones = limit / 100
percent = limit / 100
		
maxRoundTripTime = 0
totalRoundTripTime = 0
lastMessageSent = 0
current = 1

exitEvent = threading.Event()

def on_actual_changed(map):
	global current, lastMessageSent, maxRoundTripTime, totalRoundTripTime

	if current >= limit:
		returned = node.tpdo[1]['Velocity actual value'].raw
		print(f'last bounced back value: {returned}')
		exitEvent.set()
		return

	#if current % milestones == 0:
	#	print(f'{int(current / percent)}%')

	now = time.time()
	roundTripTime = time.time() - lastMessageSent
	maxRoundTripTime = max(maxRoundTripTime, roundTripTime)
	totalRoundTripTime += roundTripTime

	node.rpdo[1]['Target velocity'].raw = current
	node.rpdo[1].transmit()

	lastMessageSent = now

	current += 1
	
def test_run():
	global current, lastMessageSent, maxRoundTripTime, totalRoundTripTime

	maxRoundTripTime = 0
	totalRoundTripTime = 0
	lastMessageSent = 0
	current = 1
		
	exitEvent.clear()
	
	lastMessageSent = time.time()	
	# kick off our test by transmitting the first packet
	node.rpdo[1]['Target velocity'].raw = 0
	node.rpdo[1].transmit()	
	
	exitEvent.wait()
		
	print(f'rounds: {limit}, total: {totalRoundTripTime} s, avg: {totalRoundTripTime / limit * 1000} ms, max: {maxRoundTripTime * 1000} ms')

node.tpdo[1].add_callback(on_actual_changed)


try:
	for i in range(0, 1000):
		print(f'test run {i}')
		test_run()
finally:
	network.disconnect()
